import { Kouzelnik } from "./hrdinove/Kouzelnik";
import { Barbar } from "./hrdinove/Barbar";
import { Nepritel } from "./nepratele/Nepritel";

function run() {
    // vytvor instanci Kouzelnika a Barbara
    const merlin = new Kouzelnik(100, 15, 150);
    const conan = new Barbar(150, 10, 2);
    const nepritel = new Nepritel(100);

    console.log(merlin);
    console.log(conan);
    console.log(nepritel);

    merlin.zautoc(nepritel);
    conan.zautoc(nepritel);

    console.log(nepritel);
}

run();
