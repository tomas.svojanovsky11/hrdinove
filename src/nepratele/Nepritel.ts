// Vytvor tridu nepritel
// zatim bude obsahovat zivoty a metodu uberZivoty

import { ZivaBytost } from "../rozhrani/ZivaBytost";

export class Nepritel implements ZivaBytost {
    zivy = true;

    constructor(private zivoty: number) {
    }

    uberZivoty(zivoty: number): void {
        this.zivoty -= zivoty;
        if (this.zivoty <= 0) {
            this.zivy = false;
        }
    }
}