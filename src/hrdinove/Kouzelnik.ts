// Bude dedit z hrdiny
// bude navic obsahovat manu

import { Hrdina } from "./Hrdina";
import { ZivaBytost } from "../rozhrani/ZivaBytost";

export class Kouzelnik extends Hrdina {
    public constructor(zivoty: number, utok: number, private mana: number) {
        super(zivoty, utok);
    }

    public zautoc(bytost: ZivaBytost): void {
        if (this.mana >= 0) {
            this.uberManu(10);
            bytost.uberZivoty(this.utok);
        }
    }

    private uberManu(pocet: number) {
        this.mana -= pocet;
    }
}