// hrdina bude mit zivoty, utok a metody zautoc, ktera bude prijimat nepritele

import { Hrdina } from "./Hrdina";
import { ZivaBytost } from "../rozhrani/ZivaBytost";

export class Barbar extends Hrdina {
    public constructor(zivoty: number, utok: number, private silaRevu: number) {
        super(zivoty, utok);
    }

    public zautoc(bytost: ZivaBytost): void {
        bytost.uberZivoty(this.utok * this.silaRevu);
    }
}